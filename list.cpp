///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about lists
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @04_07_21
///////////////////////////////////////////////////////////////////////////////
#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const{
  
   if (head == nullptr)
      return true;
   return false;
}

void DoubleLinkedList::push_front(Node* newNode){
   //newNode -> next = nullptr;
   newNode -> prev = nullptr;

   if(DoubleLinkedList::empty()){
      head = newNode;
      tail = newNode;
      count++;
   }
   else{
      newNode -> next = head;
      head -> prev = newNode;
      head = newNode;
      count++;
   }
}

void DoubleLinkedList::push_back(Node* newNode){
   newNode -> next = nullptr;
   //newNode -> prev = nullptr;

   if(DoubleLinkedList::empty()){
      head = newNode;
      tail = newNode;
      count++;
   }
   else{
      tail -> next = newNode;
      newNode -> prev = tail;
      tail = newNode;
      count++;
   }
}

Node* DoubleLinkedList::pop_front(){
   if(head == tail)
      return nullptr;

   if(head != nullptr){
      Node* temp = head;
      head = temp-> next;
      temp -> prev = nullptr; 
      count--;
      return temp;
   }
   else
      return nullptr;
}

Node* DoubleLinkedList::pop_back(){
   if(tail == head)
      return nullptr;

   if(tail != nullptr){
      Node* temp = tail;
      tail = temp-> prev; //point tail at new last node in list
      temp-> next = nullptr;
      count--;
      return temp;
   }
   else
      return nullptr;

}

Node* DoubleLinkedList::get_first() const{
   return head;
}

Node* DoubleLinkedList::get_last() const{
   return tail;
}

Node* DoubleLinkedList::get_next(const Node* currentNode) const{
   return currentNode -> next;
}

Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
   return currentNode -> prev;
}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   newNode -> prev = currentNode;
   newNode -> next = currentNode -> next;
   currentNode -> next = newNode;   //move next of currentNode as newNode
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   newNode -> prev = currentNode -> prev;
   newNode -> next = currentNode;
   currentNode -> prev = newNode;
}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   assert(node1 != nullptr);
   assert(node2 != nullptr);

   if(node1 == node2)
      return;

   Node* node1_left = node1 -> prev;
   Node* node1_right = node1 -> next;
   Node* node2_left = node2 -> prev;
   Node* node2_right = node2 -> next;

   bool isAdjoined = (node1_right == node2);
   
   //node1_left
   if(!isAdjoined)
      node1 -> prev = node2 -> prev;
   else
      node1 -> prev = node2;

   if(node1 != head){
      node1_left -> next = node2;
      node2 -> prev = node1_left;
   }
   else{
      head = node2;
      node2 -> prev = nullptr;
   }

   //node2_right
   if(!isAdjoined)
      node2 -> next = node1_right;
   else
      node2 -> next = node1;

   if(node2 != tail){
      node2_right -> prev = node1;
      node1 -> next = node2_right;
   }
   else{
      tail = node1;
      node1 -> next = nullptr;
   }

   //inside connectors
   if(!isAdjoined){
      node1_right -> prev = node2;
      node2_left -> next = node1;
   }


}

const bool DoubleLinkedList::isSorted() const{
   if(count <= 1)
      return true;

   for(Node* i = head; i-> next != nullptr; i = i->next){
      if(*i > *i -> next)
         return false;
   }
   return true;
} 

void DoubleLinkedList::insertionSort(){

   for(Node* i = head; i -> next != nullptr; i = i-> next){ 
      
      Node* minNode = i;

      for(Node* j = i-> next; j !=nullptr; j = j-> next){
         if(*minNode > *j)
         minNode = j;
      }

      DoubleLinkedList::swap(i, minNode);
      i = minNode;
   
   }

}

