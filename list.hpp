///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all list
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04_07_21
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "node.hpp"
#include <cstddef>
#include <cassert>

//class Node;

class DoubleLinkedList{
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;
      unsigned int count = 0;

   public:
      const bool empty() const;  //return true if list is empty, false if not
      
      void push_front(Node* newNode);  // Add newNode to the front of the list
      void push_back(Node* newNode);   //Add newNode to back of list
      
      Node* pop_front();   //Remove a node from front of list. if list is empty, return nullptr
      Node* pop_back();    //Remove a node from back of list. If list is empty, return nullptr
      
      Node* get_first() const;   //return first node
      Node* get_last() const;    //return very last node from list. Don't make changes
      Node* get_next(const Node* currentNode) const;  //return node immediately after currentNode
      Node* get_prev(const Node* currentNode) const;  //return node immediately before CurrentNode
      
      inline unsigned int size() const{ return count; };    // return the number of nodes in list
      
      void insert_after(Node* currentNode, Node* newNode);  //insert newNode after currentNode
      void insert_before(Node* currentNode, Node* newNode); //insert newNode before currentNode
      
      void swap(Node* node1, Node* node2);

      const bool isSorted() const;     //depends on Node's < operator
      void insertionSort();            //depends on Node's < operator
};
